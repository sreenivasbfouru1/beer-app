package org.example.model;

import lombok.*;


@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Builder

public class Beer {
    private String beerId;
    private String beerName;

}
