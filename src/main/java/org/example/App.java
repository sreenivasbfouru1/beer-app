package org.example;

import org.example.model.Beer;
import java.util.UUID;

/**
 * Hello world!
 *
 */
public class App {

    public static Beer getBeer() {
        Beer beer = Beer.builder().build();
        beer.setBeerId(UUID.randomUUID().toString());
        beer.setBeerName("cortana");
        return beer;
    }

    public static void main(String[] args) {

 System.out.println(getBeer());
    }
}